FROM node:18-alpine AS main

WORKDIR /app
COPY package.json .
RUN npm i
COPY server.js server.js

CMD node server.js
