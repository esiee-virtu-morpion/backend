const { MongoClient, ServerApiVersion } = require("mongodb");
const uri =
  "mongodb+srv://vincent:5W87ZwRlqU6tD3WJ@cluster-tic-tac-toe.tosbmi9.mongodb.net/?retryWrites=true&w=majority&appName=cluster-tic-tac-toe";

const client = new MongoClient(uri, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  },
});

const cors = require("cors");
const express = require("express");
const app = express();
app.use(express.json());
app.use(cors());
const PORT = process.env.PORT || 3000;

app.get("/scores", async (req, res) => {
  try {
    await client.connect();

    const collection = client.db("cluster-tic-tac-toe").collection("scores");

    const scores = await collection.find().toArray();

    res.json(scores);
  } catch (error) {
    console.error("Erreur lors de la récupération des scores :", error);
    res.status(500).send("Erreur lors de la récupération des scores.");
  } finally {
    await client.close();
  }
});

app.post("/scores", async (req, res) => {
  try {
    const { playerName, score } = req.body;

    await client.connect();

    const collection = client.db("cluster-tic-tac-toe").collection("scores");

    await collection.insertOne({ playerName, score });

    res.status(201).send("Score enregistré avec succès !");
  } catch (error) {
    console.error("Erreur lors de l'enregistrement du score :", error);
    res.status(500).send("Erreur lors de l'enregistrement du score.");
  } finally {
    await client.close();
  }
});

app.listen(PORT, () => {
  console.log(`Serveur en cours d'exécution sur le port ${PORT}`);
});
